#!/usr/bin/python

import sys
import base64
from pkcs7padding import *
from Crypto.Cipher import AES

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def xor_block(b1, b2):
    assert(len(b1) == len(b2))
    result = ""
    for (x, y) in zip(b1, b2):
        result += chr(ord(x) ^ ord(y))
    return result

def decrypt_aes128cbc(data, key, iv):
    plaintext = ""
    aes = AES.new(key, AES.MODE_ECB)
    for c in chunks(data, 16):
        result = aes.decrypt(c)
        plaintext += xor_block(result, iv)
        iv = c
    return plaintext

def encrypt_aes128cbc(data, key, iv):
    ciphertext = ""
    aes = AES.new(key, AES.MODE_ECB)
    for c in chunks(data, 16):
        xored = xor_block(c, iv)
        iv = aes.encrypt(xored)
        ciphertext += iv
    return ciphertext

if __name__ == "__main__":
    data = base64.b64decode(sys.stdin.read())
    key = "YELLOW SUBMARINE"
    iv = "\x00"*16

    if sys.argv[1] == "encrypt":
        sys.stdout.write(encrypt_aes128cbc(pkcs7pad(data, 16), key, iv))
    elif sys.argv[1] == "decrypt":
        sys.stdout.write(pkcs7unpad(decrypt_aes128cbc(data, key, iv)))
