#!/usr/bin/python

import sys
import struct

def pkcs7pad(data, block_size):
    pad_size = block_size - len(data) % block_size
    if pad_size == 0:
        pad_size += block_size
    data += chr(pad_size) * pad_size
    return data

def pkcs7unpad(data):
    pad_size = ord(data[-1])
    return data[:-pad_size]

if __name__ == "__main__":
    block_size = int(sys.stdin.readline(), 10)
    sys.stdout.write(pkcs7pad(sys.stdin.read(), block_size))
