#!/usr/bin/python

import sys
import base64

data = bytearray.fromhex(sys.stdin.readline()[:-1])
data = base64.b64encode(data)
print(data)
