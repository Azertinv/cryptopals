#!/usr/bin/python

import sys

def chunks(l, n):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(l), n):
        yield l[i:i + n]

def getline():
    for line in sys.stdin.readlines():
        data = bytearray.fromhex(line[:-1])
        for c1 in chunks(data, 16):
            i = 0
            for c2 in chunks(data, 16):
                if c1 == c2:
                    i+= 1
                    if i > 3:
                        return line

for c in chunks(getline(), 32):
    print c
