#!/usr/bin/python

import sys

data = bytearray.fromhex(sys.stdin.readline()[:-1])
best = ""
best_score = 0

char_freq = "ETAOINSR HDLUCMFYWGPBVKXQJZ"[::-1].lower()

for i in range(0xFF):
    score = 0
    result = ""
    for c in data:
        result += chr(c ^ i)
    for c in result:
        if c.lower() in char_freq:
            score += char_freq.index(c.lower())
    if score > best_score:
        best_score = score
        best = result

print best
