#!/usr/bin/python

import sys
import base64
from Crypto.Cipher import AES
data = base64.b64decode(sys.stdin.read())
key = "YELLOW SUBMARINE"

aes = AES.new(key, AES.MODE_ECB)
print aes.decrypt(data)[-10:]
