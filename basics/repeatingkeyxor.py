#!/usr/bin/python

import sys

key = sys.stdin.readline()[:-1]
data = sys.stdin.read()[:-1]
result = ""

i = 0
for c in data:
    result += chr(ord(c) ^ ord(key[i]))
    i += 1
    if i >= len(key):
        i = 0
for c in result:
    sys.stdout.write("%02x" % ord(c))
sys.stdout.write('\n')
