#!/usr/bin/python

import sys
import base64
import string

def hdist(a, b):
    result = 0
    assert(len(a) == len(b))
    for (x, y) in zip(a, b):
        result += bin(ord(x) ^ ord(y)).count("1")
    return result

def avg_hdist(d, s):
    return sum([float(hdist(d[i:i+s], d[i+s:i+s*2])) / s for i in range(500)])

data = base64.b64decode(sys.stdin.read())
hdist_keys = [(k, avg_hdist(data, k)) for k in range(2, 40)]
keysize = min(hdist_keys, key=lambda x: x[1])[0]
chunks = [[ord(x) for x in data[i::keysize]] for i in range(keysize)]
charset = [ord(k) for k in (string.ascii_letters + " ,'-")]
key = ""
for chunk in chunks:
    best_key = 0
    best_score = 0
    for i in range(0xFF):
        score = 10000 - sum([1 if c ^ i not in charset else 0 for c in chunk])
        if score > best_score:
            best_score = score
            best_key = i
    key += chr(best_key)
print "key ----------"
print key

result = ""

i = 0
for c in data:
    result += chr(ord(c) ^ ord(key[i]))
    i += 1
    if i >= len(key):
        i = 0
print "result -------"
print result
