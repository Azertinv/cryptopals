#!/usr/bin/python

import sys

bestbest = ""
bestbest_score = 0

for line in sys.stdin.readlines():
    data = bytearray.fromhex(line[:-1])
    best = ""
    best_score = 0

    char_freq = "ETAOINSR HDLUCM'FYWGPBVKXQJZ"[::-1].lower()

    for i in range(0xFF):
        score = 0
        result = ""
        for c in data:
            result += chr(c ^ i)
        for c in result:
            if c.lower() in char_freq:
                score += char_freq.index(c.lower())
        if score > best_score:
            best_score = score
            best = result
    if best_score > bestbest_score:
        bestbest_score = best_score
        bestbest = best
sys.stdout.write(bestbest)
