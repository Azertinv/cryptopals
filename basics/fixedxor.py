#!/usr/bin/python

import sys
import base64

data1 = bytearray.fromhex(sys.stdin.readline()[:-1])
data2 = bytearray.fromhex(sys.stdin.readline()[:-1])

result = ""

for (a, b) in zip(data1, data2):
    result += chr(a ^ b)

print(result)
